

package com.adii.homepage;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

public class homepage extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("PROXY.COM");
        primaryStage.setWidth(800);
        primaryStage.setHeight(800);

        // Load the images
        Image top = new Image(getClass().getResourceAsStream("/assets/top.png"));
        Image bottom = new Image(getClass().getResourceAsStream("/assets/bottom.png"));

        // Create the ImageView objects
        ImageView topImageView = new ImageView(top);
        ImageView bottomImageView = new ImageView(bottom);

        // Set the size of the images (optional)
        topImageView.setFitWidth(70);
        topImageView.setFitHeight(70);
        bottomImageView.setFitWidth(70);
        bottomImageView.setFitHeight(70);

        // Create the buttons with the images
        Button topRightButton = new Button();
        topRightButton.setGraphic(topImageView);

        Button bottomRightButton = new Button();
        bottomRightButton.setGraphic(bottomImageView);

        // Create a line to be placed below the top right logo
        Line line = new Line(0, 250, 1870, 250); // Adjust the width to match the image width

        // Create layouts for top and bottom sections
        VBox topRightBox = new VBox(topRightButton, line);
        topRightBox.setAlignment(Pos.TOP_RIGHT);
        topRightBox.setPadding(new Insets(10, 100, 20, 10)); // Move slightly to the left

        VBox bottomRightBox = new VBox(bottomRightButton);
        bottomRightBox.setAlignment(Pos.BOTTOM_RIGHT);
        bottomRightBox.setPadding(new Insets(10, 100, 20, 10)); // Move slightly to the left

        // Create the main layout
        BorderPane mainLayout = new BorderPane();

        // Add components to the main layout
        mainLayout.setTop(topRightBox);
        mainLayout.setBottom(bottomRightBox);

        // Create the scene and set the main layout
        Scene scene = new Scene(mainLayout);
        primaryStage.setScene(scene);
        primaryStage.show();

        // Set action (click) event for topRightButton
        topRightButton.setOnAction(event -> {
            System.out.println("teachers info");
        });

        // Set action (click) event for bottomRightButton
        bottomRightButton.setOnAction(event -> {
            System.out.println("create a class");
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}

