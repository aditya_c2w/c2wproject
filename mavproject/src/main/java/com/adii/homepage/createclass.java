/*package com.adii.homepage;


import javafx.application.Application;

import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
public class createclass extends Application {
    @Override
    public void start(Stage primarystage) throws Exception{
        primarystage.setTitle("core2web");
        primarystage.setWidth(800);
        primarystage.setHeight(800);
        

           

    }
}  */


package com.adii.homepage;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

public class createclass extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("PROXY.COM");
        primaryStage.setWidth(800);
        primaryStage.setHeight(800);

        // Create a button
        Button centerBottomButton = new Button("Create Class");

        // Set the preferred size of the button
        centerBottomButton.setPrefWidth(200);
        centerBottomButton.setPrefHeight(100);

        // Create a line to be placed above the button
        Line line = new Line(0, 0, 2000, 0); // Adjusted line width to match the button

        // Create a VBox to stack the line and the button
        VBox vbox = new VBox(10); // 10px spacing between the line and button
        vbox.getChildren().addAll(line, centerBottomButton);
        vbox.setAlignment(Pos.BOTTOM_CENTER);

        // Create a StackPane to position the VBox
        StackPane stackPane = new StackPane(vbox);

        // Create the main layout
        VBox mainLayout = new VBox();
        mainLayout.getChildren().add(stackPane);
        mainLayout.setAlignment(Pos.BOTTOM_CENTER);

        // Load background image
        Image backgroundImage = new Image(getClass().getResourceAsStream("/assets/createclass.png"));

        //  BackgroundImage
        BackgroundImage background = new BackgroundImage(
            backgroundImage,
            BackgroundRepeat.NO_REPEAT,
            BackgroundRepeat.NO_REPEAT,
            BackgroundPosition.CENTER,
            BackgroundSize.DEFAULT
        );

        // Set background to main layout
        mainLayout.setBackground(new Background(background));

        // Create the scene and set the main layout
        Scene scene = new Scene(mainLayout, 800, 800);
        primaryStage.setScene(scene);
        primaryStage.show();

        // on click event for centerBottomButton
        centerBottomButton.setOnAction(event -> {
            System.out.println("Class Information");
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
