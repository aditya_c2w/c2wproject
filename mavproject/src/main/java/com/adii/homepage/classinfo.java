

package com.adii.homepage;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;




public class classinfo extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Class Information");
        primaryStage.setWidth(400);
        primaryStage.setHeight(400);

        // Create Labels and TextFields for class information
        Label classNameLabel = new Label("Class Name:");
        TextField classNameTextField = new TextField();

        Label batchLabel = new Label("Batch:");
        TextField batchTextField = new TextField();

        Label subjectLabel = new Label("Subject:");
        TextField subjectTextField = new TextField();

        Label teacherNameLabel = new Label("Teacher Name:");
        TextField teacherNameTextField = new TextField();

        Label codeLabel = new Label("Code:");
        TextField codeTextField = new TextField();

        // Create a VBox to hold Labels and TextFields
        VBox vbox = new VBox(10); // 10px spacing between nodes
        vbox.setPadding(new Insets(20)); // Add padding around the VBox
        vbox.getChildren().addAll(
                classNameLabel, classNameTextField,
                batchLabel, batchTextField,
                subjectLabel, subjectTextField,
                teacherNameLabel, teacherNameTextField,
                codeLabel, codeTextField
        );
        

        // Create a Button
        Button button = new Button("Submit");

        // Set action (click) event for the Button
        button.setOnAction(event -> {
            String className = classNameTextField.getText();
            String batch = batchTextField.getText();
            String subject = subjectTextField.getText();
            String teacherName = teacherNameTextField.getText();
            String code = codeTextField.getText();

            System.out.println("Class Name: " + className);
            System.out.println("Batch: " + batch);
            System.out.println("Subject: " + subject);
            System.out.println("Teacher Name: " + teacherName);
            System.out.println("Code: " + code);
        });

        // Add the Button to the VBox
        vbox.getChildren().add(button);

        // Create the Scene
        Scene scene = new Scene(vbox, 50, 50);

        // Set the Scene and show the Stage
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
